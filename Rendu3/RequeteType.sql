--Consulter les annonces 


--Ajouter un avis
--Ajout d'un avis et d'une note
INSERT INTO Avis (note, commentaire, signalement, immatriculation, pseudonyme)
VALUES (3,'Assez bon véhicule, mais l''allume cigare est cassé !', FALSE,'AB123CD', 'loc1');
--L'Utilisateur décide de mettre un signalement apres coup, il peut modifier son avis avec cette requette :
UPDATE Avis
SET signalement = 'True', commentaire = 'nul en fait', signalement = 'TRUE', note = 1
WHERE immatriculation = 'AB123CD' AND pseudonyme = 'loc1';

--Reserver une voiture
INSERT INTO ContratLocation (id_contrat, debut, fin, franchise,prix_litre, prix_kilometrage, kilometrage_max, pseudo_locataire, immat_vehicule, id_etatdeslieu_initial)
VALUES 
(0, '2024-05-01', '2024-05-15', 'sans reduction',1.5, 0.10, 500, 'loc1', 'AB123CD', 106);

--Consulter les annonces de véhicules et faire des recherches personnalisées selon plusieurs critères
--Avec marque spécifique, qui peut rouler en france.
SELECT * FROM Vehicule v
JOIN PeutRouler p ON p.immatriculation = v.immatriculation
WHERE( v.marque = 'Renault' OR v.marque = 'Peugeot' OR  v.marque ='Toyota') AND p.nom = 'France';

--Avec moins de 3 signalement
SELECT immatriculation FROM Avis
GROUP BY immatriculation
HAVING SUM(CASE WHEN signalement THEN 1 ELSE 0 END) < 3;

--Récupérer des statistiques à partir du parc automobile
--Moyenne des ages
SELECT AVG(age) FROM (SELECT age FROM Locataire UNION ALL SELECT age From Proprietaire) AS Comb_Age;

--Moyenne des compteur kilométriques des voitures
SELECT AVG(kilometrage) FROM Vehicule;

--Les catégories les plus utilisés
SELECT categorie, COUNT(categorie) FROM Vehicule 
GROUP BY categorie
ORDER BY COUNT(categorie) DESC
LIMIT 3;