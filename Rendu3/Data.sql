INSERT INTO Proprietaire (pseudonyme, nom, prenom, age, telephone, mail, adresse)
VALUES 
('prop1', 'Dupont', 'Jean', 45, 123456789, 'jean.dupont@example.com', '123 rue de la Liberté'),
('prop2', 'Durand', 'Marie', 40, 987654321, 'marie.durand@example.com', '456 avenue de la Paix');

--Création des Véhicules
INSERT INTO Vehicule (immatriculation, kilometrage, carburant, categorie, marque, modele, couleur, pseudonyme_proprietaire)
VALUES 
('AB123CD', 155630, 30.7, 'Citadine','Renault', 'Clio', 'Bleu', 'prop1'),
('XY987ZY', 200563, 25.3, 'Pick-up','Peugeot', '208', 'Rouge', 'prop2'),
('CD345EF', 96365, 45.2,'Hybride','Citroen', 'C3', 'Noir', 'prop1'),
('FG678GH', 132548, 36.2,'Hybride','Toyota', 'Yaris', 'Vert', 'prop2'),
('HI901IJ', 230475, 37.0,'Citadine','Ford', 'Fiesta', 'Blanc', 'prop1'),
('JK123LM', 162331, 40.6,'Citadine','Volkswagen', 'Polo', 'Gris', 'prop2'),
('MN456OP', 305162, 28.1, '4x4','Honda', 'Civic', 'Jaune', 'prop1');

--Creation Locataire 
INSERT INTO Locataire (pseudonyme, nom, prenom, age, telephone, mail, adresse, date_validité_permis, id_permis)
VALUES 
('loc1', 'Lefebvre', 'Alice', 30, 234567891, 'alice.lefebvre@example.com', '789 boulevard des Entrepreneurs', '2025-12-31', 'P123456789'),
('loc2', 'Moreau', 'Sophie', 35, 345678912, 'sophie.moreau@example.com', '159 rue des Orfèvres', '2026-01-31', 'P234567890'),
('loc3', 'Girard', 'Éric', 29, 456789123, 'eric.girard@example.com', '260 rue du Grand Chêne', '2027-02-28', 'P345678901');

--Création entreprises 
INSERT INTO Entreprise (pseudonyme, nom, prenom, age, telephone, mail, adresse, ciret)
VALUES 
('ent1', 'QuickRent', 'Support', 5, 345678912, 'contact@quickrent.com', '1010 chemin des Affaires', '12345678901234'),
('ent2', 'SpeedyRent', 'Operations', 10, 456789123, 'info@speedyrent.com', '2020 avenue de l''Industrie', '98765432101234'),
('ent3', 'EcoRent', 'Service', 8, 567891234, 'support@ecorent.com', '3030 chemin de la Durabilité', '87654321012345');

-- Insertion d'un état des lieux d'entrée pour chaque véhicule loué
INSERT INTO EtatDesLieux (id_etatdeslieux, date, quantitee_carburant, kilometrage)
VALUES 
(101, '2024-06-01', 0.75, 12345),  -- Associé au véhicule immatriculé CD345EF
(102, '2024-06-01', 0.65, 54321),  -- Associé au véhicule immatriculé HI901IJ
(103, '2024-06-01', 0.70, 23456),  -- Associé au véhicule immatriculé JK123LM
(104, '2024-07-01', 0.80, 34567),  -- Associé au véhicule immatriculé FG678GH
(105, '2024-07-16', 0.85, 45678),  -- Associé au véhicule immatriculé MN456OP
(106, '2024-05-01', 0.80, 45678),  -- Associé au véhicule immatriculé AB123CD
(107, '2024-05-16', 0.70, 45678);  -- Associé au véhicule immatriculé XY987ZY

-- Contrats pour locataires
INSERT INTO ContratLocation (id_contrat, debut, fin, franchise,prix_litre, prix_kilometrage, kilometrage_max, pseudo_locataire, immat_vehicule, id_etatdeslieu_initial)
VALUES 
(0, '2024-05-01', '2024-05-15', 'sans reduction',1.5, 0.10, 500, 'loc1', 'AB123CD', 106),
(1, '2024-07-01', '2024-07-15', 'franchise reduite',1.4, 0.13, 350, 'loc2', 'FG678GH', 104),
(2, '2024-07-16', '2024-07-30', '0 franchise',1.3, 0.14, 450, 'loc3', 'MN456OP', 105);

-- Contrats pour entreprises
INSERT INTO ContratLocation (id_contrat, debut, fin, franchise,prix_litre, prix_kilometrage, kilometrage_max, pseudo_entreprise, immat_vehicule, id_etatdeslieu_initial)
VALUES 
(3, '2024-05-16', '2024-05-30', 'sans reduction', 1.5, 0.10, 800, 'ent1', 'XY987ZY', 107),
(4, '2024-05-18', '2024-06-12', '0 franchise', 1.2, 0.12, 900, 'ent2', 'CD345EF', 101),
(5, '2024-06-01', '2024-06-15', 'sans reduction', 1.6, 0.11, 400, 'ent3', 'HI901IJ', 102),
(6, '2024-06-01', '2024-06-15', 'franchise reduite', 1.6, 0.11, 400, 'ent3', 'JK123LM', 103);

-- Insertion d'un état des lieux de sortie pour chaque véhicule
INSERT INTO EtatDesLieux (id_etatdeslieux, date, quantitee_carburant, kilometrage)
VALUES 
(108, '2024-06-15', 0.50, 12500),  -- Associé au véhicule immatriculé CD345EF
(109, '2024-06-15', 0.55, 55000),  -- Associé au véhicule immatriculé HI901IJ
(110, '2024-06-15', 0.60, 24000),  -- Associé au véhicule immatriculé JK123LM
(111, '2024-07-15', 0.40, 35000),  -- Associé au véhicule immatriculé FG678GH
(112, '2024-07-30', 0.45, 46000),  -- Associé au véhicule immatriculé MN456OP
(113, '2024-05-15', 0.80, 45678),  -- Associé au véhicule immatriculé AB123CD
(114, '2024-05-30', 0.70, 45678);  -- Associé au véhicule immatriculé XY987ZY

--Pays
INSERT INTO Pays (nom)
VALUES 
('France'),
('Belgique'),
('Allemagne');

--Pays
INSERT INTO PeutRouler (immatriculation,nom)
VALUES 
('AB123CD','France'),
('FG678GH','France'),
('HI901IJ','France'),
('XY987ZY','Belgique'),
('CD345EF','Allemagne');

--Avis
INSERT INTO Avis (note, commentaire, signalement, immatriculation, pseudonyme)
VALUES (3,'Assez bon véhicule, mais l''allume cigare est cassé !', True,'AB123CD', 'loc1'),
(3,'Assez bon véhicule, mais l''allume cigare est cassé !', True,'AB123CD', 'loc2'),
(3,'Assez bon véhicule, mais l''allume cigare est cassé !', True,'AB123CD', 'loc3'),
(3,'Assez bon véhicule, mais l''allume cigare est cassé !', False,'FG678GH', 'loc3');