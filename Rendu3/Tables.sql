DROP TABLE IF EXISTS Pays CASCADE ;
DROP TABLE IF EXISTS Avis CASCADE ;
DROP TABLE IF EXISTS Proprietaire CASCADE ;
DROP TABLE IF EXISTS Entreprise CASCADE ;
DROP TABLE IF EXISTS Locataire CASCADE ;
DROP TABLE IF EXISTS Conducteur CASCADE ;
DROP TABLE IF EXISTS Vehicule CASCADE ;
DROP TABLE IF EXISTS PeriodeDisponibilite CASCADE ;
DROP TABLE IF EXISTS PeutRouler CASCADE ;
DROP TABLE IF EXISTS Degat CASCADE ;
DROP TABLE IF EXISTS EtatDesLieux CASCADE ;
DROP TABLE IF EXISTS contratlocation CASCADE ;
DROP TYPE IF EXISTS ModeDePayement CASCADE ;
DROP TYPE IF EXISTS Criticitee CASCADE ;
DROP TYPE IF EXISTS FRANCHISE CASCADE ;

CREATE TYPE ModeDePayement as ENUM ('Carte', 'Chèques', 'Liquide');
CREATE TYPE Criticitee as ENUM ('Pas de dégât', 'Dégât majeur', 'Dégât mineur');
CREATE TYPE FRANCHISE as ENUM('sans reduction', 'franchise reduite', '0 franchise');
CREATE TABLE Pays (
  nom VARCHAR PRIMARY KEY
);

CREATE TABLE Proprietaire (
  pseudonyme VARCHAR PRIMARY KEY,
  nom VARCHAR NOT NULL,
  prenom VARCHAR NOT NULL,
  age INTEGER NOT NULL,
  url_photo VARCHAR ,
  telephone INTEGER NOT NULL UNIQUE,
  mail VARCHAR NOT NULL,
  adresse VARCHAR NOT NULL
);

CREATE TABLE Entreprise (
  pseudonyme VARCHAR PRIMARY KEY,
  nom VARCHAR NOT NULL, 
  prenom VARCHAR NOT NULL,
  age INTEGER NOT NULL,
  url_photo VARCHAR ,
  telephone INTEGER NOT NULL UNIQUE,
  mail VARCHAR NOT NULL,
  adresse VARCHAR NOT NULL,
  ciret VARCHAR NOT NULL UNIQUE 
) ;

CREATE TABLE Locataire (
  pseudonyme VARCHAR PRIMARY KEY, 
  nom VARCHAR NOT NULL,
  prenom VARCHAR NOT NULL,
  age INTEGER NOT NULL,
  url_photo VARCHAR ,
  telephone INTEGER NOT NULL UNIQUE,
  mail VARCHAR NOT NULL,
  adresse VARCHAR NOT NULL,
  id_permis VARCHAR NOT NULL UNIQUE, 
  date_validité_permis DATE -- CHECK ( date_validité_permis > CURDATE() )
);

CREATE TABLE Conducteur (
  pseudonyme VARCHAR PRIMARY KEY,
  nom VARCHAR,
  prenom VARCHAR,
  age INTEGER,
  date_validite_permis DATE,
  id_permis VARCHAR,
  pseudonyme_employeur VARCHAR NOT NULL,
  FOREIGN KEY (pseudonyme_employeur) REFERENCES Entreprise(pseudonyme)
);

CREATE TABLE Vehicule (
  immatriculation VARCHAR PRIMARY KEY,
  annee_mise_circulation DATE,
  kilometrage INTEGER,
  carburant FLOAT,
  categorie VARCHAR,
  marque VARCHAR,
  modele VARCHAR,
  couleur VARCHAR,
  option VARCHAR,
  description VARCHAR,
  num_assurance INTEGER,
  date_expiration_assurance DATE,
  pseudonyme_proprietaire VARCHAR NOT NULL,
  FOREIGN KEY (pseudonyme_proprietaire) REFERENCES Proprietaire(pseudonyme)
);


CREATE TABLE Avis (
  note INTEGER,
  commentaire TEXT,
  signalement BOOLEAN,
  immatriculation VARCHAR,
  pseudonyme VARCHAR,
  PRIMARY KEY (immatriculation, pseudonyme),
  FOREIGN KEY (immatriculation) REFERENCES Vehicule(immatriculation),
  FOREIGN KEY (pseudonyme) REFERENCES Locataire(pseudonyme)
);

CREATE TABLE PeriodeDisponibilite (
  vehicule_immatriculation VARCHAR NOT NULL,
  debut DATE,
  fin DATE,
  PRIMARY KEY (vehicule_immatriculation, debut, fin),
  FOREIGN KEY (vehicule_immatriculation) REFERENCES Vehicule(immatriculation)
);

CREATE TABLE PeutRouler ( 
  immatriculation VARCHAR NOT NULL,
  nom VARCHAR NOT NULL,
  PRIMARY KEY (immatriculation, nom),
  FOREIGN KEY (immatriculation) REFERENCES Vehicule(immatriculation),
  FOREIGN KEY (nom) REFERENCES Pays(nom) 
) ;

CREATE TABLE EtatDesLieux(
  date DATE NOT NULL,
  id_etatdeslieux INTEGER PRIMARY KEY,
  quantitee_carburant FLOAT(10) NOT NULL,
  kilometrage INTEGER NOT NULL
);

CREATE TABLE Degat(
  nom_element_voiture VARCHAR(50),
  criticité CRITICITEE,
  commentaire VARCHAR(50),
  id INTEGER PRIMARY KEY,
  id_etatdeslieux INTEGER NOT NULL,
  FOREIGN KEY (id_etatdeslieux) REFERENCES EtatDesLieux(id_etatdeslieux)
);

CREATE TABLE ContratLocation (
  id_contrat INTEGER PRIMARY KEY,
  debut DATE,
  fin DATE,
  franchise FRANCHISE,
  prix_litre FLOAT,
  prix_kilometrage FLOAT,
  kilometrage_max INTEGER,
  pseudo_locataire VARCHAR,
  pseudo_entreprise VARCHAR,
  immat_vehicule VARCHAR NOT NULL,
  id_etatdeslieu_initial INTEGER NOT NULL,
  UNIQUE (debut, fin,pseudo_locataire, pseudo_entreprise, immat_vehicule ),
  FOREIGN KEY (pseudo_locataire) REFERENCES Locataire(pseudonyme),
  FOREIGN KEY (pseudo_entreprise) REFERENCES Entreprise(pseudonyme),
  CHECK ( (pseudo_locataire IS NOT NULL AND pseudo_entreprise IS NULL) OR 
         (pseudo_entreprise IS NOT NULL AND pseudo_locataire IS NULL) ),
  FOREIGN KEY (immat_vehicule) REFERENCES Vehicule(immatriculation),
  FOREIGN KEY (id_etatdeslieu_initial) REFERENCES EtatDesLieux(id_etatdeslieux)
);