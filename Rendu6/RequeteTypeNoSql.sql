--Consulter les annonces 


--Ajouter un avis
--Ajout d'un avis et d'une note
INSERT INTO Avis (note, commentaire, signalement, immatriculation, pseudonyme)
VALUES (3,'Assez bon véhicule, mais l''allume cigare est cassé !', FALSE,'AB123CD', 'loc1');
--L'Utilisateur décide de mettre un signalement apres coup, il peut modifier son avis avec cette requette :
UPDATE Avis
SET signalement = 'True', commentaire = 'nul en fait', note = 1
WHERE immatriculation = 'AB123CD' AND pseudonyme = 'loc1';

--Reserver une voiture
-- Contrats pour locataires
INSERT INTO ContratLocation (id_contrat, debut, fin, franchise,prix_litre, prix_kilometrage, kilometrage_max, pseudo_locataire, immat_vehicule, etatdeslieu_initial)
VALUES 
(0, '2024-05-01', '2024-05-15', 'sans reduction',1.5, 0.10, 500, 'loc1', 'AB123CD',
'{"date":"20-01-2024","quantitee_carburant":20.3,"kilometrage":100200,
    "degats":[
                {"nom_element_voiture":"parechocs","criticité":"Degat mineur","commentaire":"Détaché."},
                {"nom_element_voiture":"parebrise","criticité":"Degat majeur","commentaire":"Brisé"}
            ],
    "PhotosVoiture":[{"url":"https://www.jmautos-casse-auto.fr/photodynboutique2/370/390/non,kg1f5cq6-DSCF8875.JPG"}]
}'
);

-- Finir un Contrat en ajoutant une facture
UPDATE ContratLocation
SET facture =
'{"date":"20-02-2024","kilometrage":100300, "ModeDePayement":"Carte",
    "etatdeslieuxfinal":{"date":"20-01-2024","quantitee_carburant":20.3,"kilometrage":100200,
        "degats":[
                    {"nom_element_voiture":"parechocs","criticité":"Degat mineur","commentaire":"Détaché."},
                    {"nom_element_voiture":"parebrise","criticité":"Degat majeur","commentaire":"Brisé"}
                ],
        "PhotosVoiture":[{"url":"https://www.jmautos-casse-auto.fr/photodynboutique2/370/390/non,kg1f5cq6-DSCF8875.JPG"}]
    } 
}'
WHERE id_contrat = 0;

--Retrouver la photo de l'etat des lieux final de la facture du contrat
SELECT facture->'etatdeslieuxfinal'->'PhotosVoiture'->0->'url' AS URLPhoto FROM ContratLocation WHERE id_contrat = 0;


--Consulter les annonces de véhicules et faire des recherches personnalisées selon plusieurs critères
--Avec marque spécifique, qui peut rouler en france.
SELECT * FROM Vehicule v
JOIN PeutRouler p ON p.immatriculation = v.immatriculation
WHERE( v.marque = 'Renault' OR v.marque = 'Peugeot' OR  v.marque ='Toyota') AND p.nom = 'France';

--Les voiture disponible le ...
SELECT immatriculation, periode_disponibilites->0 AS periodeDisponibilites FROM Vehicule;

/*

*/

--Avec moins de 3 signalement
SELECT immatriculation FROM Avis
GROUP BY immatriculation
HAVING SUM(CASE WHEN signalement THEN 1 ELSE 0 END) < 3;

--Récupérer des statistiques à partir du parc automobile
--Moyenne des ages
SELECT AVG(age) FROM (SELECT age FROM Locataire UNION ALL SELECT age From Proprietaire) AS Comb_Age;

--Moyenne des compteur kilométriques des voitures
SELECT AVG(kilometrage) FROM Vehicule;

--Les catégories les plus utilisés
SELECT categorie, COUNT(categorie) FROM Vehicule 
GROUP BY categorie
ORDER BY COUNT(categorie) DESC
LIMIT 3;