INSERT INTO Proprietaire (pseudonyme, nom, prenom, age, telephone, mail, adresse)
VALUES 
('prop1', 'Dupont', 'Jean', 45, 123456789, 'jean.dupont@example.com', '123 rue de la Liberté'),
('prop2', 'Durand', 'Marie', 40, 987654321, 'marie.durand@example.com', '456 avenue de la Paix');

--Création des Véhicules
INSERT INTO Vehicule (immatriculation, kilometrage, carburant, categorie, marque, modele, couleur, pseudonyme_proprietaire,periode_disponibilites)
VALUES 
('AB123CD', 155630, 30.7, 'Citadine','Renault', 'Clio', 'Bleu', 'prop1','[{"debut":"02-06-2023", "fin": "03-12-2025"},{"debut":"03-12-2025", "fin": "13-12-2025"}]'),
('XY987ZY', 200563, 25.3, 'Pick-up','Peugeot', '208', 'Rouge', 'prop2', '[]'),
('CD345EF', 96365, 45.2,'Hybride','Citroen', 'C3', 'Noir', 'prop1', '[]'),
('FG678GH', 132548, 36.2,'Hybride','Toyota', 'Yaris', 'Vert', 'prop2', '[]'),
('HI901IJ', 230475, 37.0,'Citadine','Ford', 'Fiesta', 'Blanc', 'prop1', '[]'),
('JK123LM', 162331, 40.6,'Citadine','Volkswagen', 'Polo', 'Gris', 'prop2', '[]'),
('MN456OP', 305162, 28.1, '4x4','Honda', 'Civic', 'Jaune', 'prop1', '[]');

--Creation Locataire 
INSERT INTO Locataire (pseudonyme, nom, prenom, age, telephone, mail, adresse, date_validité_permis, id_permis)
VALUES 
('loc1', 'Lefebvre', 'Alice', 30, 234567891, 'alice.lefebvre@example.com', '789 boulevard des Entrepreneurs', '2025-12-31', 'P123456789'),
('loc2', 'Moreau', 'Sophie', 35, 345678912, 'sophie.moreau@example.com', '159 rue des Orfèvres', '2026-01-31', 'P234567890'),
('loc3', 'Girard', 'Éric', 29, 456789123, 'eric.girard@example.com', '260 rue du Grand Chêne', '2027-02-28', 'P345678901');

--Création entreprises 
INSERT INTO Entreprise (pseudonyme, nom, prenom, age, telephone, mail, adresse, ciret, conducteurs)
VALUES 
('ent1', 'QuickRent', 'Support', 5, 345678912, 'contact@quickrent.com', '1010 chemin des Affaires', '12345678901234',
'[{"pseudonyme":"a","nom":"Gastro","prenom":"Fidella","age":"50","date_validite_permis":"30-06-2025","id_permis":"5352223sq"},
{"pseudonyme":"b","nom":"Macron","prenom":"Manu","age":"45","date_validite_permis":"20-07-2025","id_permis":"qssq223sq"}]'),
('ent2', 'SpeedyRent', 'Operations', 10, 456789123, 'info@speedyrent.com', '2020 avenue de l''Industrie', '98765432101234','[]'),
('ent3', 'EcoRent', 'Service', 8, 567891234, 'support@ecorent.com', '3030 chemin de la Durabilité', '87654321012345','[]');

-- Contrats pour locataires
INSERT INTO ContratLocation (id_contrat, debut, fin, franchise,prix_litre, prix_kilometrage, kilometrage_max, pseudo_locataire, immat_vehicule, etatdeslieu_initial, facture)
VALUES 
(0, '2024-05-01', '2024-05-15', 'sans reduction',1.5, 0.10, 500, 'loc1', 'AB123CD',
'{"date":"20-01-2024","quantitee_carburant":20.3,"kilometrage":100200,
    "degats":[
                {"nom_element_voiture":"parechocs","criticité":"Degat mineur","commentaire":"Détaché."},
                {"nom_element_voiture":"parebrise","criticité":"Degat majeur","commentaire":"Brisé"}
            ],
    "PhotosVoiture":[{"url":"https://www.jmautos-casse-auto.fr/photodynboutique2/370/390/non,kg1f5cq6-DSCF8875.JPG"}]
}',
'{"date":"20-02-2024","kilometrage":100300, "ModeDePayement":"Carte",
    "etatdeslieuxfinal":{"date":"20-01-2024","quantitee_carburant":20.3,"kilometrage":100200,
        "degats":[
                    {"nom_element_voiture":"parechocs","criticité":"Degat mineur","commentaire":"Détaché."},
                    {"nom_element_voiture":"parebrise","criticité":"Degat majeur","commentaire":"Brisé"}
                ],
        "PhotosVoiture":[{"url":"https://www.jmautos-casse-auto.fr/photodynboutique2/370/390/non,kg1f5cq6-DSCF8875.JPG"}]
    } 
}');

--Pays
INSERT INTO Pays (nom)
VALUES 
('France'),
('Belgique'),
('Allemagne');

--Pays
INSERT INTO PeutRouler (immatriculation,nom)
VALUES 
('AB123CD','France'),
('FG678GH','France'),
('HI901IJ','France'),
('XY987ZY','Belgique'),
('CD345EF','Allemagne');

--Avis
INSERT INTO Avis (note, commentaire, signalement, immatriculation, pseudonyme)
VALUES (3,'Assez bon véhicule, mais l''allume cigare est cassé !', True,'AB123CD', 'loc1'),
(3,'Assez bon véhicule, mais l''allume cigare est cassé !', True,'AB123CD', 'loc2'),
(3,'Assez bon véhicule, mais l''allume cigare est cassé !', True,'AB123CD', 'loc3'),
(3,'Assez bon véhicule, mais l''allume cigare est cassé !', False,'FG678GH', 'loc3');